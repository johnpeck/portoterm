# Set up a visual console for logging
ttk::labelframe .console -text "[dict get $state thisprog] execution log"\
    -labelanchor n \
    -borderwidth 1 \
    -relief groove

# Set up the text widget.  Specify -width in units of characters in
# the -font option
text .console.text -yscrollcommand {.console.scrl set} \
    -width 100 \
    -height 10 \
    -font LogFont 

# Use yview for a vertical scrollbar -- scrolls in the y direction
# based on input
scrollbar .console.scrl -orient vertical -command {.console.text yview}

# initialize logger subsystems
# two loggers are created
# 1. main
# 2. a separate logger for plugins
set log [logger::init main]
set log [logger::init global]
${::log}::setlevel $loglevel; # Set the log level

proc log.newfile {} {
    # Get a new filename for the execution log
    global log
    global state
    set first_logfile "[dict get $state thisprog].log"
    set logfile $first_logfile
    set suffixnum 1
    while {[file exists $logfile]} {
	set logfile [file rootname ${first_logfile}]_${suffixnum}.log
	incr suffixnum
    }
    return $logfile
}
    
proc log.send_to_file {txt} {
    global log
    global state
    if {[string equal [dict get $state exelog] "none"]} {
	set logfile [log.newfile]
	dict set state exelog $logfile
    } else {
	set logfile [dict get $state exelog]
    }
    set f [open $logfile a+]
    fconfigure $f -encoding utf-8
    puts $f $txt
    close $f
}

# Send log messages to wherever they need to go
proc log_manager {lvl txt} {
    set msg "\[[clock format [clock seconds]]\] $txt"
    # The logfile output
    log.send_to_file $msg
    
    # The console logger output.  Mark the level names and color them
    # after the text has been inserted.
    if {[string compare $lvl debug] == 0} {
	# Debug level logging
    	set msg "\[ $lvl \] $txt \n"
    	.console.text insert end $msg
    	.console.text tag add debugtag \
	    {insert linestart -1 lines +2 chars} \
	    {insert linestart -1 lines +7 chars}
    	.console.text tag configure debugtag -foreground blue
    }
    if {[string compare $lvl info] == 0} {
	# Info level logging
    	set msg "\[ $lvl \] $txt \n"
    	.console.text insert end $msg
    	.console.text tag add infotag \
	    {insert linestart -1 lines +2 chars} \
	    {insert linestart -1 lines +7 chars}
    	.console.text tag configure infotag -foreground green
    }
    if {[string compare $lvl warn] == 0} {
	# Warn level logging
    	set msg "\[ $lvl \] $txt \n"
    	.console.text insert end $msg
    	.console.text tag add warntag \
	    {insert linestart -1 lines +2 chars} \
	    {insert linestart -1 lines +7 chars}
    	.console.text tag configure warntag -foreground orange
    }
    if {[string compare $lvl error] == 0} {
	# Error level logging
    	set msg "\[ $lvl \] $txt \n"
    	.console.text insert end $msg
    	.console.text tag add errortag \
	    {insert linestart -1 lines +2 chars} \
	    {insert linestart -1 lines +7 chars}
    	.console.text tag configure errortag -foreground red
    }
    # Scroll to the end
    .console.text see end
}

# Define the callback function for the logger for each log level
foreach lvl [logger::levels] {
    interp alias {} log_manager_$lvl {} log_manager $lvl
    ${log}::logproc $lvl log_manager_$lvl
}

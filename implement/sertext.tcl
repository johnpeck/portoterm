set {sertext.logfile} "lastlog.log"

# Set up the serial console
ttk::labelframe .sertext -text "Serial Teriminal"\
    -labelanchor n\
    -borderwidth 1\
    -relief groove
# Set up the text widget.  Specify -width in units of characters in
# the -font option
text .sertext.text -yscrollcommand {.sertext.scrl set} \
    -width 100 \
    -height 20 \
    -font LogFont 
# Use yview for a vertical scrollbar -- scrolls in the y direction
# based on input
scrollbar .sertext.scrl -orient vertical -command {.sertext.text yview}


proc sertext.newfile {} {
    # Get a new filename for the serial output log
    global log
    global state
    set shortport [file tail [dict get $state portname]]
    set first_logfile "serial_${shortport}.log"
    set logfile $first_logfile
    set suffixnum 1
    while {[file exists $logfile]} {
	set logfile [file rootname ${first_logfile}]_${suffixnum}.log
	incr suffixnum
    }
    return $logfile
}



proc sertext.log_to_file {txt} {
    global log
    global state
    if {[string equal [dict get $state serlog] "none"]} {
	set logfile [sertext.newfile]
	dict set state serlog $logfile
	${log}::info "Logging serial output to $logfile"
    } else {
	set logfile [dict get $state serlog]
    }
    set f [open ${logfile} a+]
    fconfigure $f -encoding utf-8
    puts $f $txt
    close $f
}

proc sertext.cornerchar {} {
    # Return the character at the lower-left corner of the text entry
    # box.
    global log
    set cornerchar [.sertext.text get "current lineend -1 chars"]
    return $cornerchar
}

# Send serial text messages to wherever they need to go
proc sertext_manager {text} {
    global state

    if !{[dict get $state stopped]} {
	# We're doing data collection -- logging data to the datafile
	# Add a timestamp to the message for the datafile
	set filemsg "\[[clock format [clock seconds]]\] $text"
	sertext.log_to_file $filemsg
	# Add an extra newline to the visible text message
	set textmsg "\[[clock format [clock seconds] -format %T]\]  $text\n"
	.sertext.text insert end $textmsg
	# Scroll to the end
	.sertext.text see end
    }
}

proc sertext.redline {text} {
    .sertext.text insert end $text redtag
    .sertext.text tag configure redtag -foreground red
    .sertext.text see end
}

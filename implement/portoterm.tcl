
# --------------------- Global configuration --------------------------


# By default, the software will look for the configuration file in the
# directory from which it was launched.  If the configuration file is
# not found, one will be created.
set configfile "portoterm.cfg"

# The base filename for the execution log.  The actual filename will add
# a number after this to make a unique logfile name.
set execution_logbase "portoterm"

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

# Set the log level.  Known values are:
# debug
# info
# notice
# warn
# error
# critical
# alert
# emergency
set loglevel debug

#----------------------- Done with global configuration ---------------
package require Tk
set thisprog [file rootname [file tail [info script]]]

# Create a dictionary to keep track of global state
# State variables:
#   thisprog --  Name of this program (for naming the window)
#   thisos  -- Name of the os this program is running on
#   stopped -- Saving to results file has been stopped
#   portname -- The name of the channel the tester is connected to
#   portchan -- The tcl channel name the tester is connected to
#   exelog -- The execution log filename
#   serlog -- The serial output log filename
set state [dict create \
	       thisprog [file rootname [file tail [info script]]] \
	       thisos $tcl_platform(os) \
	       stopped 0 \
	       portname none \
	       portchan none \
	       exelog none \
	       serlog none 
	  ]

proc modinfo {modname} {
    # Return loaded module details.
    set modver [package require $modname]
    set modlist [package ifneeded $modname $modver]
    set modpath [lindex $modlist end]
    return "Loaded $modname module version $modver from ${modpath}."
}


# -------------------------- Set up fonts -----------------------------

# This has to come before the logger setup, since the logger needs
# them
source fonts.tcl

#----------------------------- Set up logger --------------------------

# The logging system will use the console text widget for visual
# logging.

package require logger
source loggerconf.tcl
${log}::info [modinfo logger]

# Testing the logger

.console.text insert end "Current loglevel is: [${log}::currentloglevel] \n"
${log}::info "Trying to log to [dict get $state exelog]"
${log}::info "Known log levels: [logger::levels]"
${log}::info "Known services: [logger::services]"
${log}::debug "Debug message"
${log}::info "Info message"
${log}::warn "Warn message"


# Can't tell which version of Tk we're using until the logger is set
# up.
${log}::info [modinfo Tk]
${log}::debug "Running on [dict get $state thisos]"

# -------------------------- Root window ------------------------------

menu .menubar
menu .menubar.help -tearoff 0
.menubar add cascade -label Help -menu .menubar.help -underline 0
. configure -menu .menubar -width 200 -height 150
.menubar.help add command -label "About [dict get $state thisprog]..." \
    -underline 0 -command help.about

# Create window icon
set wmiconfile icons/swlogo_xpar.png
set wmicon [image create photo -format png -file $wmiconfile]
wm iconphoto . $wmicon

proc help.about {} {
    # What to execute when Help-->About is selected
    #
    # Arguments:
    #   None
    global log
    global revcode
    global state
    tk_messageBox -message "[dict get $state thisprog]\nVersion $revcode" \
	-title "About [dict get $state thisprog]"
}

# ------------------- Set up configuration file -----------------------

package require inifile
${log}::info [modinfo inifile]
source config.tcl

proc config.init {} {
    # Write an initial configuration file.  This will be
    # project-dependent, so it can't go in the config.tcl library.
    #
    # Arguments:
    #   None
    global log
    global revcode
    global configfile
    set fcon [ini::open $configfile w]
    # ---------------------- Private section --------------------------
    ini::set $fcon private version $revcode
    ini::comment $fcon private "" "Internal use -- do not edit."
    ini::commit $fcon
    ini::close $fcon
    # -------------------- bitlabels section --------------------------
    config.seccom bitlabels "Labels for individual bits"
}


	
if {[file exists $configfile] == 0} {
    # The config file does not exist
    ${log}::info "Creating new configuration file [file normalize $configfile]"
    set fcon [ini::open $configfile w]
    ini::close $fcon
    config.init
} else {
    ${log}::info "Reading configuration file [file normalize $configfile]"
    set fcon [ini::open $configfile r]
    ${log}::info "Configuration file version is\
                  [ini::value $fcon private version]"
    ini::close $fcon
}

# ------------------- Set up serial terminal --------------------------
source sertext.tcl


# ----------------------- Connection node -----------------------------

ttk::labelframe .serport -text "Serial port" \
    -labelanchor n\
    -borderwidth 1\
    -relief sunken
ttk::labelframe .serport.connection_frme -text "Connection" \
    -labelanchor n\
    -borderwidth 1\
    -relief sunken
ttk::label .serport.connection_frme.port_labl -text "none" -font NameFont
ttk::button .serport.reset_bttn -text "Reset" \
    -command serport.reset
ttk::button .serport.scan_bttn -text "Scan" \
    -command serport.scan


# --------------------- Stop / go buttons -----------------------------
set startstop_bttn_label "Stop"
ttk::button .startstop_bttn -textvariable startstop_bttn_label \
    -command startstop 

proc startstop {} {
    global log
    global state
    global state_stopped
    global startstop_bttn_label
    if {[dict get $state stopped]} {
	# We're running
	dict set state stopped 0
	set startstop_bttn_label "Stop"
    } else {
	# We've paused
	dict set state stopped 1
	set startstop_bttn_label "Go"
    }
}


# ---------------------- Position widgets -----------------------------

# Serial port information
grid config .serport -column 0 -row 0 \
    -columnspan 1 -rowspan 1 \
    -padx 10 -pady 10 \
    -sticky "snew"
pack .serport.connection_frme \
    -padx 10 -pady 10 \
    -side left \
    -expand 1
pack .serport.connection_frme.port_labl
pack .serport.reset_bttn \
    -padx 10 -pady 10 \
    -side right \
    -expand 1
pack .serport.scan_bttn \
    -padx 10 -pady 10 \
    -side right \
    -expand 1

grid config .startstop_bttn -column 0 -row 1 \
    -columnspan 1 -rowspan 1 \
    -padx 10 -pady 10

set sertext_row 2
grid config .sertext -column 0 -row $sertext_row \
    -columnspan 1 -rowspan 1 \
    -padx 10 -pady 10 \
    -sticky "snew"
pack .sertext.scrl -fill y -side right
pack .sertext.text -fill x -side bottom -fill both -expand true

grid config .console -column 0 -row 3 \
    -columnspan 1 -rowspan 1 \
    -padx 10 -pady 10 \
    -sticky "snew"
pack .console.scrl -fill y -side right
pack .console.text -fill x -side bottom -fill both -expand true

# Set up grid for resizing
#
# Column 0 gets the extra space
grid columnconfigure . 0 -weight 1
# Allow the serial terminal to expand
grid rowconfigure . $sertext_row -weight 1



# --------------------------- Do stuff --------------------------------

proc serport.init {node} {
    # Returns either the serial port channel or None
    #
    # Arguments:
    #   node -- String used by fconfigure to set up serial port
    global log
    set mode "115200,n,8,1"
    if {[catch {set portchan [open ${node} r+]}]} {
	# The port simply doesn't exist
	${log}::debug "Failed to open serial port at $node"
	set portchan none
    } else {
	${log}::debug "Trying serial port at $node"
	chan configure $portchan -mode $mode -buffering none -blocking 0 -translation binary
	# Wait for the opening greeting to scroll in...
	after 500
	# Bulk read all of the initial greeting and menu
	set data [chan read $portchan]
	if {[string first "Hello World!" $data] > 0} {
	    # This is a genuine master dongle
	    ${log}::debug "Found Master Dongle at $node"
	    .sertext.text insert end $data
	    .sertext.text see end
	} else {
	    ${log}::debug "Serial port at $node is not connected to a free Master Dongle"
	    set portchan none
	}
    }
    return $portchan
}

proc serport.reset {} {
    global log
    global state
    if {[string equal [dict get $state portname] "none"]} {
	return
    } else {
	set serport [dict get $state portchan]
	# Hold dtr low for 500ms to trigger the arduino's autoreset
	${log}::warn "Resetting connection to [dict get $state portname]"
	# Disable the serial port readable event
	chan event $serport readable ""
	chan configure $serport -ttycontrol {dtr 0}
	after 500
	chan configure $serport -ttycontrol {dtr 1}
	after 500
	# Bulk read all of the initial greeting and menu
	set data [chan read $serport]
	.sertext.text insert end $data
	.sertext.text see end
	connection.update
    }
}

proc serport.makelist {} {
    # Returns a list of strings used by fconfigure to set up serial ports
    #
    # Arguments:
    #  None
    global log
    global state
    set maxports 10
    set serportlist [list]
    if {[string equal [dict get $state thisos] Linux]} {
	# Platform is Linux.  Look through /dev/ttyUSB entries
	foreach entry [split [glob -nocomplain -directory /dev ttyUSB*]] {
	    lappend serportlist $entry
	}
    } elseif {[string equal [dict get $state thisos] "Darwin"]} {
	# Platform is OSX.  Look through /dev/tty.usbmodem and /dev/tty.usbserial entries
	foreach entry [split [glob -nocomplain -directory /dev tty.usbmodem*]] {
	    lappend serportlist $entry
	}
	foreach entry [split [glob -nocomplain -directory /dev tty.usbserial*]] {
	    lappend serportlist $entry
	}
    } elseif {[string equal [dict get $state thisos] "Windows NT"]} {
	# Platform is Windows.  Entry will be a com port
	for {set comnum 1} {$comnum < $maxports} {incr comnum} {
	    lappend serportlist com$comnum
	}
    }
    return $serportlist
}

proc serport.readline {serport} {
    global log
    global state
    if {[catch {set data [chan gets $serport]}]} {
	${log}::error "Failed to read from [dict get $state portname]"
	chan close $serport
	dict set state portname "none"
	dict set state portchan "none"
    } elseif {[eof $serport]} {
	# The USB cable was unplugged or the device died
	${log}::error "Connection at [dict get $state portname] is broken"
	${log}::info "Closing connection to [dict get $state portname]"
	sertext.redline "Connection to [dict get $state portname] broken -- trying to reconnect"
	chan close $serport
	dict set state portname "none"
	dict set state portchan "none"
	# Try reconnecting
	serport.scan
    } else {
	# Refuse to write zero-length strings
	if [expr [string length $data] > 0] {
	    ${log}::debug "Writing $data"
	    sertext_manager $data
	}
    }
    connection.update
}

proc connection.update {} {
    # Update serial port connection information
    global log
    global state
    if {[string equal [dict get $state portname] "none"]} {
	# There's no connection.
	.serport.connection_frme.port_labl configure -text "none" -foreground red
    } else {
	# We have a working serial port
	.serport.connection_frme.port_labl configure \
	    -text [dict get $state portname] \
	    -foreground green
	after cancel serport.scan
	# Get text line by line as it becomes available
	chan event [dict get $state portchan] readable {serport.readline [dict get $state portchan]}
	# If any key is pressed to enter a character into the serial text box,
	# send that key's character to the serial port.  Use the KeyRelease
	# binding so that a newline can be inserted in the serial monitor
	# after the keystroke appears.
	bind .sertext.text <KeyRelease> {serport.sendchar %A}
    }
    # Update the window title
    wm title . "[dict get $state thisprog] -- [dict get $state portname]"
}

proc serport.scan {} {
    # Scan for serial ports, connect if you find a Master Dongle
    global log
    global state
    set portlist [serport.makelist]
    if {[llength $portlist] != 0} {
	# portlist will have zero length if no ports were detected
	foreach portentry $portlist {
	    set serport [serport.init $portentry]
	    if {[string equal $serport none]} {
		dict set state portname "none"
		dict set state portchan "none"
		connection.update
		if {[string equal $portentry [lindex $portlist end]]} {
		    ${log}::error "No Master Dongles detected"
		}
	    } else {
		# We've found a Master Dongle
		dict set state portname $portentry
		dict set state portchan $serport
		connection.update
		break
	    }
	}
    } else {
	# There were no ports detected
	${log}::error "No serial ports detected"
	dict set state portname "none"
	dict set state portchan "none"
	connection.update
    }
    # if {[string equal [dict get $state portname] "none"]} {
    # 	# If we didn't find a Master Dongle, periodically search again
    # 	${log}::debug "Scheduling serial port scan"
    # 	after 100 serport.scan
    # }
}


proc serport.sendchar {character} {
    global log
    global state
    ${log}::debug "Sending $character to the serial port"
    chan puts [dict get $state portchan] "$character"
    .sertext.text insert end "\n"
}

# Start by scanning serial ports
serport.scan












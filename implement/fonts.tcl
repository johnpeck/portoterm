# fonts for the console logger.
font create FixedFont -family TkFixedFont -size 12

# Font for names of things
font create NameFont -family TkFixedFont -size 10 -weight bold

# Font for console log
font create LogFont -family TkFixedFont -size 8 
